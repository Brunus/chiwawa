#!/usr/bin/python

'''
Chiwawa monitor a tree directory and take actions in case of file movements.

Author : Brunus
- Mastodon account : https://framapiaf.org/@Brunus
- Diaspora account : brunus@framasphere.org

Licence : Chiwawa is published under MIT Licence

Requirements :

- Watchdog python library:
https://pypi.python.org/pypi/watchdog

- logmailer script in case you want to mail alerts and log file

'''

import os, sys, json, argparse, time, logging

from datetime import datetime, timedelta

try:
    import logmailer
except ImportError:
    print "Warning ! logmailer is not present at side of chiwawa."
    print "Impossible to mail alerts and log file."

try :
    from watchdog.observers import Observer
    from watchdog.events import PatternMatchingEventHandler
    from watchdog.events import FileSystemEventHandler, \
         DirMovedEvent, DirDeletedEvent, DirCreatedEvent, DirModifiedEvent, \
        FileMovedEvent, FileDeletedEvent, FileCreatedEvent, FileModifiedEvent
except ImportError:
    print "Watchdog library is missing."
    sys.exit(1)

try :
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # for Python ver. < 3.0


parser = argparse.ArgumentParser(description='Chiwawa monitor a tree directory and take actions in case of file movements.')
parser.add_argument('-i', '--inifile', help='path to the configuration file (default: %(default)s)', default="chiwawa.ini", required=False)
args = parser.parse_args()


# setting some params before filling it with values coming from config file

sendMail = False
deleteFile = False
config = {}  # will receive configuration params

# initialising timers

start_time = datetime.now()
current_time = datetime.now()


# configuration initialisation

def config_init(iniFile):

    if not os.path.isfile(iniFile):
        print 'no ' + iniFile + ' was found. Please provide one.'
        print 'stopping chiwawa'
        sys.exit(1)
    
    else:

        Dict = {}

        config = ConfigParser()
        config.read(iniFile)
        watchPatterns = json.loads(config.get('basic_configuration', 'watchPatterns'))
        watchPathes = json.loads(config.get('basic_configuration', 'watchPathes'))
        ignorePathesWith = json.loads(config.get('basic_configuration', 'ignorePathesWith'))
        logFile = config.get('basic_configuration', 'logFile')
        defenseMode = config.get('basic_configuration', 'defenseMode')
        mailerMode = config.get('mailer_configuration', 'mailerMode')
        mailerRange = config.getint('mailer_configuration', 'mailerRange')
        
        Dict.update({
            'watchPatterns':watchPatterns,
            'watchPathes':watchPathes,
            'ignorePathesWith':ignorePathesWith,
            'logFile':logFile,
            'defenseMode':defenseMode,
            'mailerMode':mailerMode,
            'mailerRange':mailerRange
            })
        return(Dict)


# logging initialisation

def logging_init():
    logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename= config['logFile'],
                    filemode='w')

    # set up the console logging
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


# checking if directories listed in watchPathes exists

def directories_check():
    
    for watchPath in config['watchPathes'] :
        if not os.path.isdir(watchPath):
            logging.info('no ' + watchPath + ' was found.')
            logging.info('stopping chiwawa')
            sys.exit(1)


# sending start infos to console

def start_infos():
    
    logging.info('log file : ' + config['logFile'])
    logging.info("monitored patterns :")
    for watchPattern in config['watchPatterns']:
        logging.info(watchPattern)
    logging.info("monitored directories :")
    for watchPath in config['watchPathes'] :
        logging.info(watchPath)
    logging.info("excluded patterns :")
    for ignorePath in config['ignorePathesWith']:
        logging.info(ignorePath)
    logging.info('defense mode is : ' + config['defenseMode'])


# processing Watchdog events

class MyEventHandler(FileSystemEventHandler):

    def dispatch(self,event):

        ignoreEvent = False

        if isinstance(event, FileCreatedEvent):
            for ignorePath in config['ignorePathesWith']:
                if ( ignorePath in event.src_path.split('/')):
                    ignoreEvent = True        
            
            if ignoreEvent != True:
                check_file('created', event.src_path, False)
                ignoreEvent = False

        elif isinstance(event, FileModifiedEvent):
            for ignorePath in config['ignorePathesWith']:
                if ( ignorePath in event.src_path.split('/')):
                    ignoreEvent = True        
            
            if ignoreEvent != True:
                check_file('modified', event.src_path, False)
                ignoreEvent = False
                    
        elif isinstance(event, FileMovedEvent):
            for ignorePath in config['ignorePathesWith']:
                if ( ignorePath in event.src_path.split('/')):
                    ignoreEvent = True        
            
            if ignoreEvent != True:
                check_file('moved', event.src_path, event.dest_path)
                ignoreEvent = False


# take actions if defenseMode is on

def delete_file(src_file):
    
    if (deleteFile == True):
        os.remove(src_file)
        logging.info('Deleted file : '+ src_file)


# send mail if mailer mode is on and range wide enough between last mail en now

def send_mail(start_time, current_time):
    
    current_time = datetime.now()
    if (sendMail == True and (current_time - start_time > timedelta(minutes = config['mailerRange']))):
        logmailer.create_mailer(config['logFile'])
        start_time = datetime.now()

# checking if src_file or dest_file are in monitored patterns and initiate actions

def check_file(event_type, src_file, dest_file):
    
    if event_type == 'created' :
        for pattern in config['watchPatterns']:
            if (src_file.endswith(pattern)):
                logging.info(src_file + ' created')
                delete_file(src_file)
                send_mail(start_time, current_time)

    # in case of modified only file we log and alert but we don't delete the file
    
    elif event_type == 'modified' :
        for pattern in config['watchPatterns']:
            if (src_file.endswith(pattern)):
                logging.info(src_file + ' modified')
                send_mail(start_time, current_time)
    
    # in case of moved file we check the dest_file instead of the src_file
    
    elif event_type == "moved" :
        for pattern in config['watchPatterns']:
            if (dest_file.endswith(pattern)):
                logging.info(src_file + ' moved to ' + dest_file)
                delete_file(dest_file)
                send_mail(start_time, current_time)


# chiwawa initialisation

def chiwawa():

    logging.info("Initiating chiwawa")

    event_handler = MyEventHandler()
    observer = Observer()
    for watchPath in config['watchPathes'] :
        observer.schedule(event_handler, watchPath, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()    

if __name__ == "__main__":

    config.update(config_init(args.inifile))
    
    if(config['defenseMode'] == 'on'):
        deleteFile = True

    if(config['mailerMode'] == 'on'):
        sendMail = True

    logging_init()
    start_infos()
    directories_check()
    logging.info("watched directories check : OK")
    
    # initiating chiwawa
    chiwawa()
