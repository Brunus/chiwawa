#!/usr/bin/python

'''
Log-mailer is a file mailer I wrote ton send log files

Author : Brunus
- Mastodon account : https://framapiaf.org/@Brunus
- Diaspora account : brunus@framasphere.org

Licence : log-mailer is published under MIT Licence

'''

import os
import time
import argparse
import json
import logging
import sys
import smtplib
from subprocess import call

from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

try :
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # for Python ver. < 3.0


parser = argparse.ArgumentParser(description='logmailer is a simple script to mail files.')
parser.add_argument('-l', '--logfile', help='path to the file to send (default: %(default)s)', required=False)
parser.add_argument('-i', '--inifile', help='path to the configuration file (default: %(default)s)', default="logmailer.ini", required=False)
args = parser.parse_args()

# setting up things reading logmailer's config file

config = ConfigParser()
config.read(args.inifile)
mailer_method = config.get('mailer_configuration', 'mailer_method')
send_from = config.get('mailer_configuration', 'send_from')
sender_password = config.get('mailer_configuration', 'sender_password')
send_to = json.loads(config.get('mailer_configuration', 'send_to'))
smtp_Host = config.get('mailer_configuration', 'smtp_Host')
smtp_Port = config.getint('mailer_configuration', 'smtp_port')
mail_subject = config.get('mailer_configuration', 'mail_subject')

class MailerSMTP():

    def __init__(self, logFile):

        self.msg = MIMEMultipart()
        self.msg['Subject'] = mail_subject
        self.msg['From'] = send_from
        self.msg['To'] = COMMASPACE.join(send_to)

        self.part = MIMEBase('application', "octet-stream")
        self.part.set_payload(open(logFile, 'rb').read())
        Encoders.encode_base64(self.part)
        self.part.add_header('Content-Disposition', 'attachment; filename="%s"' % logFile)

        self.msg.attach(self.part)
        

        # Send the message via your own SMTP server, but don't include the
        # envelope header.

        print self.msg # comment this after testing
        
        ### uncomment the next lines when you are ready to go production ###
        '''
        self.server = smtplib.SMTP(smtp_Host,smtp_Port)
        self.server.set_debuglevel(1)
        self.server.ehlo()
        self.server.login(send_from, sender_password)
        self.server.ehlo()
        self.server.sendmail(send_from, send_to, self.msg.as_string())
        self.server.quit()
        '''
        ####################################################################

class MailerMutt():

    def __init__(self, logFile):

        self.commandLine = 'echo | mutt -s "' + mail_subject + '" -a ' + logFile + ' -- ' 
        self.commandLine += ','.join(send_to)
        call(self.commandLine, shell=True)


def create_mailer(logFile):

    if (mailer_method == 'mutt'):
        mailer = MailerMutt(logFile)

    else :
        mailer = MailerSMTP(logFile)


if __name__ == "__main__":

    # create_mailer(logFile)

    try :
        create_mailer(logFile)
    
    except:
        
        if (args.logfile):
            create_mailer(args.logfile)

        else :

            print "ERROR : no file provided !"
            print "You need to provide a file to send."
            print "You can choose to :"
            print "- provide it by using the option --logfile path/filename.log"
            print "- using logmailer as an import in another python script and giving him the path to a file to send."
    