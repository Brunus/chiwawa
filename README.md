# chiwawa
Chiwawa is a Python script made to learn and practice but also to monitor directories like websites trees, detect files movements, and even take actions.

As you know, web softwares are often weakened by security flows.
Pirates or script kiddies using those security flows are sometimes able to upload files on servers on the purpose to use it as remote attack tools.

Chiwawa can track every file movement in a directory tree, it could be usefull not only to monitor websites trees.
In the settings I'm providing I'm focusing on .ph and .js files but it's up to you to use chiwawa to track other extensions.

In the configuration file you can decide to use the delete function, making Chiwawa able to delete an unwanted file like a .php created in the watched tree (it works with fake images files renamed .php by the attacker when he wants yo use it).

You also could use the log mailer, to receive e-mail alerts, check the configuration file to use this function.

Chiwawa is based on the Python library Watchdog. Then it's able to run on Linux but also BSD systems.

Files in this repository :

- chiwawa.py : monitoring script
- chiwawa.ini : configuration file for chiwawa
- logmailer.py : imported in chiwawa or another script, used as standalone, it mails files
- logmailer.ini : configuration file for logmailer

To run Chiwawa as a daemon I use Supervisord, A Process Control System : http://supervisord.org/

*******************************************************************************************************

EDIT : Possible error on Linux when the parameter Inotify max user watches is too low.

If Chiwawa don't start and complain avec the value of cat inotify max_user_watches param :

cat /proc/sys/fs/inotify/max_user_watches to read the actual value

sysctl fs.inotify.max_user_watches=524288 to change this value temporarly

echo 524288 | sudo tee -a /proc/sys/fs/inotify/max_user_watches to change this value permanently

Source and more detail on usage of this parameter : http://unix.stackexchange.com/questions/13751/kernel-inotify-watch-limit-reached#13757
